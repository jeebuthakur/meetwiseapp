package Automation.Meetwise;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.touch.offset.PointOption;

public class MeetwiseApp extends AndroidCap
{
    public static void main(String[] args) throws MalformedURLException, InterruptedException 
    {
	AndroidDriver<AndroidElement> driver=Capabilities();
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	Thread.sleep(9000);
	//driver.findElementById("com.meetwise:id/nextBtn").click();
	//driver.findElementById("com.meetwise:id/nextBtn").click();
	//driver.findElementById("com.meetwise:id/nextBtn").click();
	//driver.findElementById("com.meetwise:id/nextBtn").click();
	driver.findElementById("com.meetwise:id/skipBtn").click();

	driver.findElementById("com.meetwise:id/editText").sendKeys("7837736422");
	driver.hideKeyboard();
	driver.findElementById("com.meetwise:id/loginBtn").click();
	driver.findElementById("com.meetwise:id/otpView").sendKeys("123456");
	driver.findElementById("com.meetwise:id/homeBtn").click();

	//************************************Create New Feed Module***********************************
	driver.findElementById("com.meetwise:id/createBtn").click();
	driver.findElementById("com.meetwise:id/feedLayout").click();
	driver.findElementById("com.meetwise:id/titleValueView").sendKeys("Beautiful Nature");
	driver.findElementById("com.meetwise:id/addMediaBtn").click();
	driver.findElementById("com.android.packageinstaller:id/permission_allow_button").click();
	driver.findElementById("com.meetwise:id/addMediaBtn").click();
	driver.findElementById("com.meetwise:id/iv_photo").click();
	driver.findElementByXPath("//*[@class='android.widget.ImageView' and @bounds='[720,325][1080,685]']").click();
	driver.findElementById("com.meetwise:id/addMediaBtn").click();
	driver.findElementById("com.meetwise:id/iv_photo").click();
	driver.findElementByXPath("//*[@class='android.widget.ImageView' and @bounds='[360,685][720,1045]']").click();
	driver.findElementById("com.meetwise:id/createBtn").click();		
	Thread.sleep(3000);
	//***********************************************ENd OF Feed Module*************************************

	//************************************Starting Create New Meeting Module***********************************

	driver.findElementById("com.meetwise:id/createBtn").click();
	driver.findElementById("com.meetwise:id/meetingButton").click();
	driver.findElementByXPath("//android.widget.EditText[@text='Name your meeting…']").sendKeys("Auto Testing");
	driver.findElementById("com.meetwise:id/dateArrow").click();
	driver.findElementById("com.meetwise:id/tomorrow").click();
	driver.findElementByXPath("//android.view.View[@index='2']").click();
	driver.findElementByXPath("//*[@class='android.view.View' and @bounds='[238,1508][843,1618]']");
	driver.findElementById("com.meetwise:id/applyBtn").click();
	Thread.sleep(3000);
	driver.findElementById("com.meetwise:id/endTimeLL").click();
	driver.findElementById("com.meetwise:id/applyBtn").click();
	driver.findElementById("com.meetwise:id/locValueView").click();
	driver.findElementByXPath("//android.view.ViewGroup[@index='1']").click();
	driver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textMatches(\"ADVANCE OPTIONS\").instance(0))"));  
	driver.findElementById("com.meetwise:id/advanceBtn").click();
	driver.findElementById("com.meetwise:id/descValueView").sendKeys("If you use this site regularly and would like to help keep the site on the Internet, please consider donating a small sum to help pay for the hosting and bandwidth bill.");
	driver.findElementById("com.meetwise:id/workBtn").click();
	driver.findElementById("com.meetwise:id/tagEdTxt").sendKeys("Ui Discussion");
	driver.findElementById("com.meetwise:id/addEventBtn").click();
	driver.findElementById("com.meetwise:id/addBannerBtn").click();
	driver.findElementById("com.meetwise:id/iv_photo").click();
	driver.findElementByXPath("//*[@class='android.widget.ImageView' and @bounds='[720,325][1080,685]']").click();
	driver.findElementById("com.meetwise:id/addBannerBtn").click();
	driver.findElementById("com.meetwise:id/iv_photo").click();
	driver.findElementByXPath("//*[@class='android.widget.ImageView' and @bounds='[360,685][720,1045]']").click();
	driver.findElementById("com.meetwise:id/addMediaBtn").click();
	driver.findElementById("com.meetwise:id/iv_photo").click();
	driver.findElementByXPath("//*[@class='android.widget.ImageView' and @bounds='[0,685][360,1045]']").click();
	driver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textMatches(\"CREATE MEETING\").instance(0))"));
	// driver.findElementById("com.meetwise:id/addDocBtn").click();//Working Code per Screen Manage Ni hori
	// driver.findElementByXPath("//*[@class='android.widget.RelativeLayout' and @bounds='[0,457][1080,623]']").click();//Working Code per Screen Manage Ni hori
	driver.findElementById("com.meetwise:id/linkValueView").sendKeys("https://www.youtube.com/watch?v=B0QEU9gxapk");
	driver.findElementById("com.meetwise:id/createBtn").click();
	driver.findElementByXPath("//android.widget.Button[@text='DONE']").click();

	//***********************************************ENd OF Meeting Module*************************************

	//************************************Starting Create New Event Module***********************************
	/*Thread.sleep(9000);
	    driver.findElementById("com.meetwise:id/createBtn").click();
	   driver.findElementById("com.meetwise:id/eventsLayout").click();
	   driver.findElementById("com.meetwise:id/titleValueView").sendKeys("Bachelor Party");
       driver.findElementById("com.meetwise:id/dateArrow").click();
       driver.findElementById("com.meetwise:id/tomorrow").click();
       driver.findElementById("com.meetwise:id/enddateArrow").click();
       driver.findElementById("com.meetwise:id/endtomorrow").click();
       driver.findElementById("com.meetwise:id/startTimeLL").click();
       driver.findElementById("com.meetwise:id/applyBtn").click();
       driver.findElementById("com.meetwise:id/endTimeLL").click();
       driver.findElementById("com.meetwise:id/applyBtn").click();
       driver.findElementById("com.meetwise:id/publicBtn").click();
        driver.findElementById("com.meetwise:id/locLayout").click();
       driver.findElementByXPath("//android.view.ViewGroup[@index='1']").click();
       driver.findElementById("com.meetwise:id/freeBtn").click();
       driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.meetwise:id/advanceBtn\")).scrollIntoView(new UiSelector().textMatches(\"ADVANCE OPTIONS\").instance(0))"); 
       //driver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textMatches(\"ADVANCE OPTIONS\").instance(0))"));
       driver.findElementById("com.meetwise:id/advanceBtn").click();
       driver.findElementById("com.meetwise:id/descValueView").sendKeys("A bachelor party, also known as a stag weekend, stag do or stag party, or a buck's night is a party held for a man who is shortly to enter marriage.");
       driver.findElementById("com.meetwise:id/tagEdTxt").sendKeys("Party On The Rocks");
       driver.findElementById("com.meetwise:id/addEventBtn").click();
       driver.findElementById("com.meetwise:id/addBannerBtn").click();
       driver.findElementById("com.meetwise:id/iv_photo").click();
	    driver.findElementByXPath("//*[@class='android.widget.ImageView' and @bounds='[720,325][1080,685]']").click();
	    driver.findElementById("com.meetwise:id/addMediaBtn").click();
	    driver.findElementById("com.meetwise:id/iv_photo").click();
	    driver.findElementByXPath("//*[@class='android.widget.ImageView' and @bounds='[0,685][360,1045]']").click();
	    driver.findElementById("com.meetwise:id/addDocBtn").click();
	    driver.findElementByXPath("//*[@class='android.widget.RelativeLayout' and @bounds='[0,457][1080,623]']").click();
	    driver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textMatches(\"Video Call Link\").instance(0))"));
       driver.findElementById("com.meetwise:id/linkValueView").sendKeys("https://www.youtube.com/watch?v=nAY0wm3L47Q");
	   driver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textMatches(\"DISCARD\").instance(0))"));
	   driver.findElementById("com.meetwise:id/createBtn").click();
	   driver.findElementById("com.meetwise:id/yesBtn").click();*/

    }
}